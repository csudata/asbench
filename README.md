# ASBench

## 介绍

ASBench:  Another Sysbench, 由[中启乘数科技](http://www.csudata.com) 改造SysBench，更好的支持Oracle的数据库压测工具。

SysBench0.5的版本还支持Oracle，但是新的SysBench1.0把对Oracle的支持去掉了，
当想做Oracle、MySQL、PostgreSQL数据库做对比测试不是很方便，为此从sysbench0.5的
版本中分支出来一个版本，提供Oracle、MySQL、PostgreSQL的测试功能。


此版本还解决了Sysbench0.5对oracle做测试的某些情况下的coredump的问题，同时在我们[release页面](https://gitee.com/csudata/asbench/releases/v0.1)中 为PostgreSQL、MySQL、Oracle编译好了asbench，方便大家直接下载使用。


如需要交流或技术支持，请加微信号：CS-success 然后由此微信号拉你进入到 微信交流群中。

或关注公众号：
![公众号](csudata_qr_code_small.jpg)



## 编译方法

方法：

```
./autogen.sh
./configure --with-oracle
make ORA_LIBS="-L/usr/lib/oracle/19.9/client64/lib -lclntsh" ORA_CFLAGS="-I /usr/include/oracle/19.9/client64"
make install
```

上面的命令中“/usr/lib/oracle/19.9/client64/lib”和“/usr/include/oracle/19.9/client64”是oracle-instant安装的位置，如果你安装的不是这个位置，请修改。


## 安装和测试


到release中找到asbench的发行版本：

asbenchXX.tar.xz

解压开，有目录asbenchXX，下面有一些文件：

* asbench_mysql
* asbench_ora11
* asbench_ora19
* asbench_pg
* bench_ora11.sh
* bench_ora19.sh
* bench_pg.sh
* lua
* tnsnames.ora

### 测试Oracle

如果主机上没有安装Oracle客户端程序，把[release0.1版本中的](https://gitee.com/csudata/asbench/releases/v0.1) 中的oracle-instant11.2.tar下载下来，然后解压，解压后有oracle-instantclient的一些rpm包，安装这些rpm包:

```
rpm -ivh oracle-instantclient11.2-basic-11.2.0.4.0-1.x86_64.rpm oracle-instantclient11.2-devel-11.2.0.4.0-1.x86_64.rpm oracle-instantclient11.2-sqlplus-11.2.0.4.0-1.x86_64.rpm
```


修改bench_ora11.sh中的一些内容：

bench_ora11.sh中的Oracle的环境变量，默认安装oracle-instant11.2就是下面的这些目录下，如果你不是按我们的方法安装的，路径可能不是下面的路径，请修改：

```
export LD_LIBRARY_PATH=/usr/lib/oracle/11.2/client64/lib:$LD_LIBRARY_PATH
export PATH=/usr/lib/oracle/11.2/client64/bin:$PATH
```

bench_ora12.sh中的环境变量TNS_ADMIN, 如果你解压的目录不是/home/postgres/asbench0.1，请修改：

```
export TNS_ADMIN=/home/postgres/asbench0.1
```

修改asbench与测试相关的一些命令杭参数：
* --test=lua/select.lua： 是指定测试项，select.lua是一个按主键随机查询的测试
* --oracle-db=oradb: 自定Oracle的连接服务名，这里的oradb需要在tnsnames.ora中的一个名称
* --oracle-user=sysbench: 测试使用的数据库用户名称
* --oracle-password=sysbench: 测试使用的数据库用户的密码

在tnsnames.ora中配置数据库的连接：

```
oradb =
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.166.125)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = nocdb)
    )
  )
```

其中上面的192.168.166.125是Oracle数据库的地址，1521是oracle数据库的端口

在Oracle数据库中建测试用户sysbench:

```
create user sysbench identified by sysbench default tablespace users;
```
上面的测试用户的密码设置的为sysbench，如果你设置的密码不是sysbench，请不要忘了请修改bench_ora11.sh中的 `--oracle-password=sysbench`这一行中的密码。

做好上面的配置后，就可以先初始化测试数据：

```
./bench_ora prepare
```

实际运行的情况：

```
[codetest@pgdev asbench0.1]$ ./bench_ora11.sh prepare
asbench 0.6:  multi-threaded system evaluation benchmark

Creating table 'sbtest1'...
Inserting 100000 records into 'sbtest1'
```


运行压测：
```
./bench_ora11 run
```

实际的运行情况：

```
[codetest@pgdev asbench0.1]$ ./bench_ora11.sh run
asbench 0.6:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 4
Report intermediate results every 1 second(s)
Random number generator seed is 0 and will be ignored


Threads started!

[   1s] threads: 4, tps: 0.00, reads/s: 86.86, writes/s: 0.00, response time: 39.18ms (95%)
[   2s] threads: 4, tps: 0.00, reads/s: 381.93, writes/s: 0.00, response time: 20.83ms (95%)
[   3s] threads: 4, tps: 0.00, reads/s: 673.98, writes/s: 0.00, response time: 14.83ms (95%)
[   4s] threads: 4, tps: 0.00, reads/s: 714.09, writes/s: 0.00, response time: 12.37ms (95%)
```
